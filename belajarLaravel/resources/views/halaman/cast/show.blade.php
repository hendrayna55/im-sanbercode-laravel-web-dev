@extends('layout.master')

@section('judul')
    Show Cast
@endsection

@section('content')
    <a href="{{url('/cast/' . $cast->id . '/edit')}}" class="btn btn-sm btn-success float-right mb-3 ml-1">Edit</a>
    <a href="{{url('/cast')}}" class="btn btn-sm btn-info float-right mb-3 mr-1">Kembali</a>
    <table class="table table-bordered table-striped">
        <tr>
            <th colspan=2 class="align-middle text-center bg-info">Info Detail Cast</th>
        </tr>
        <tr>
            <th>Nama</th>
            <td>{{$cast->nama}}</td>
        </tr>
        <tr>
            <th>Umur</th>
            <td>{{$cast->umur}} Tahun</td>
        </tr>
        <tr>
            <th>Bio</th>
            <td>{{$cast->bio}}</td>
        </tr>
    </table>
@endsection