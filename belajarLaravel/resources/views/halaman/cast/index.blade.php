@extends('layout.master')

@section('judul')
    Halaman Cast
@endsection

@push('scripts')
    <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
@endpush

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
    <a href="{{url('/cast/create')}}" class="btn btn-success mb-2 btn-sm float-right">Tambah</a>
    <table id="example1" class="table table-bordered table-striped">
        <thead class="bg-info">
            <tr class="align-middle text-center">
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($casts as $cast)
                <tr class="align-middle">
                    <td><a href="{{url('/cast/' . $cast->id)}}">{{$cast->nama}}</a></td>
                    <td class="text-center">{{$cast->umur}}</td>
                    <td>{{$cast->bio}}</td>
                    <td class="text-center">
                        <div class="row justify-content-center">
                            <div class="col-4">
                                <a href="{{url('/cast/' . $cast->id)}}" class="btn btn-sm btn-success">
                                    <i class="fas fa-eye"></i>
                                </a>
                            </div>

                            <div class="col-4">
                                <a href="{{url('/cast/' . $cast->id . '/edit')}}" class="btn btn-sm btn-success">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </div>

                            <div class="col-4">
                                <form action="{{url('/cast/' . $cast->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger ml-1">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>

                            </div>

                        </div>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection