@extends('layout.master')

@section('judul')
    Halaman Create Cast
@endsection

@section('content')
    <form action="{{url('/cast')}}" method="post">
        @csrf

        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Cast">
        </div>

        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" id="umur" name="umur" placeholder="Umur Cast">
        </div>

        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" id="bio" name="bio" rows="3" placeholder="Bio Cast"></textarea>
        </div>
        
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-success float-right" type="submit">Tambah</button>
    </form>
@endsection