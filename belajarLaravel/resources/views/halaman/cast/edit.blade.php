@extends('layout.master')

@section('judul')
    Halaman Edit Cast
@endsection

@section('content')
    <form action="{{url('/cast/'. $cast->id)}}" method="POST">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Cast" value="{{$cast->nama}}">
        </div>

        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" id="umur" name="umur" placeholder="Umur Cast" value="{{$cast->umur}}">
        </div>

        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" id="bio" name="bio" rows="3" placeholder="Bio Cast">{{$cast->bio}}</textarea>
        </div>
        
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-success float-right" type="submit">Simpan</button>
    </form>
@endsection