@extends('layout.master')

@section('judul')
    Buat Akun Baru!
@endsection

@section('content')
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <div class="row">
            <div class="col-4">
                <label for="">First name :</label><br><br>
                <input type="text" name="namaDepan"><br><br>
                <label for="">Last name :</label><br><br>
                <input type="text" name="namaBelakang"><br><br>
                <label for="">Gender :</label><br><br>
                <input type="radio" name="gender" value="Male">Male<br>
                <input type="radio" name="gender" value="Female">Female<br>
                <input type="radio" name="gender" value="Other">Other<br><br>
            </div>
            <div class="col-4">
                <label for="">Nationality :</label><br><br>
                <select name="nationality">
                    <option value="Indonesia">Indonesia</option>
                    <option value="Japan">Japan</option>
                    <option value="USA">USA</option>
                    <option value="Malaysia">Malaysia</option>
                </select><br><br>
                <label for="">Languange Spoken :</label><br><br>
                <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
                <input type="checkbox" name="language" value="English">English<br>
                <input type="checkbox" name="language" value="Other">Other<br><br>
                
            </div>

            <div class="col-4">
                <label for="">Bio :</label><br><br>
                <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
            </div>
        </div>
        
        <button type="submit" class="float-right btn btn-success">Sign Up</button>
        
    </form>
@endsection