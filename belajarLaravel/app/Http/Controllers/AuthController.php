<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('halaman.form');
    }

    public function kirim(request $request){
        $namaDepan = $request['namaDepan'];
        $namaBelakang = $request['namaBelakang'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];
        // dd($request->all());

        return view('halaman.welcome', compact('namaDepan', 'namaBelakang', 'gender', 'nationality', 'language', 'bio'));
    }
}
