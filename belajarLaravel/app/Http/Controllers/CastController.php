<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cast;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('halaman.cast.index', compact('casts'));
    }

    public function create()
    {
        return view('halaman.cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = Cast::find($id);
        return view('halaman.cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('halaman.cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')
            ->where('id', $id)
            ->update([
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio
            ]);
        
        return redirect('/cast/' . $id);
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
