<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Name : $sheep->name <br>";
echo "legs : $sheep->legs<br>";
echo "cold clooded : $sheep->cold_blooded <br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>";
echo "legs : $kodok->legs<br>";
echo "cold clooded : $kodok->cold_blooded <br>";
echo "Jump : ";
$kodok->jump();

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>";
echo "legs : $sungokong->legs<br>";
echo "cold clooded : $sungokong->cold_blooded <br>";
echo "Yell : ";
$sungokong->yell();